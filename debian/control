Source: libhdhomerun
Section: libs
Priority: optional
Homepage: https://www.silicondust.com/support/linux/
Maintainer: Francois Marier <francois@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-exec (>=0.3),
 po-debconf,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/libhdhomerun.git
Vcs-Browser: https://salsa.debian.org/debian/libhdhomerun

Package: hdhomerun-config
Section: misc
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Configuration utility for Silicon Dust HD HomeRun
 This package supports the Silicon Dust HDHomeRun.
 The HDHomeRun is a networked, two tuner digital TV tuner compatible with
 MythTV, SageTV, and VLC.
 .
 This utility can be used for:
  * Discovering your tuner location and name
  * Gathering tuner settings
  * Setting tuner setting
  * Performing scans
  * Performing firmware upgrades

Package: libhdhomerun4
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Library for Silicon Dust HD HomeRun
 This package supports the Silicon Dust HDHomeRun.
 The HDHomeRun is a networked, two tuner digital TV tuner compatible with
 MythTV, SageTV, and VLC.
 .
 Shared library

Package: libhdhomerun-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 libhdhomerun4 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Development library for Silicon Dust HD HomeRun
 This package supports the Silicon Dust HDHomeRun.
 The HDHomeRun is a networked, two tuner digital TV tuner compatible with
 MythTV, SageTV, and VLC.
 .
 Development package
